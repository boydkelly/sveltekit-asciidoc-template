// svelte.config.js
import adapter from '@sveltejs/adapter-static';
import preprocess from 'svelte-preprocess'
import asciidoctor from '@asciidoctor/core';

const config = {
  kit: {
    // adapter-auto only supports some environments, see https://kit.svelte.dev/docs/adapter-auto for a list.
    // If your environment is not supported or you settled on a specific environment, switch out the adapter.
    // See https://kit.svelte.dev/docs/adapters for more information about adapters.
    adapter: adapter()
  },
  extensions: ['.svelte', '.adoc'],
  preprocess: [
    preprocess(),
    {
      async markup({ content, filename }) {
        if (filename.endsWith('.adoc')) {
          const processor = asciidoctor();
          // Configure Asciidoctor options as needed
          const html = processor.convert(content, { 
            'base_dir': 'src/routes/',
            'standalone': false,
            'safe': 'unsafe', 
            'attributes': { 
              'skip-front-matter': true,
              'showtitle': true,
              'includedir': '_include/',
              'imagesdir': '/images',
              'icons': 'font',
              'allow-uri-read': '',
            } 
          });
          // Log the filename and attributes for debugging
          console.log('Filename:', filename);
          //console.log('Attributes:', attributes);
          return {
            code: html,
          };
        }
      },
    },
  ],
};

export default config;
