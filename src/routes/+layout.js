import { error } from '@sveltejs/kit';
import Processor from '@asciidoctor/core'
const processor = Processor()

export const load = (async ({ params }) => {
  const filename = await import(`./+page.adoc?raw` /* @vite-ignore */ );
  if (filename.default) {
    const attributes = processor.load(filename.default).getAttributes()
    const doctitle = processor.load(filename.default).getDocumentTitle()
    const date =  attributes['page-revdate'];
    const doctype =  attributes['doctype'];
    const lang =  attributes['lang'];
    //console.log(attributes)
    console.log(doctitle)
    console.log(date);
    console.log(doctype);
    console.log(lang);
    return {
      date: date,
      doctype: doctype, 
      doctitle: doctitle,
      lang: lang,
      attributes
    }
  }
  throw error(404, 'Not found');
});
